**This project is now obsolete, the latest Mac SDK and demo apps can now be found in the [GlanceSDKDemos-Mac](https://gitlab.com/glance-networks/glancesdkdemos-mac) project.**

# Visitor SDK for Mac

## Setup

Before you get started you'll need a Glance account.  

Contact Glance at mobilesupport@glance.net to set up a Group (organization account) and an individual Agent account.
You will receive:
1. A numeric Group ID
2. An agent username, for example agnes.example.glance.net (Glance usernames are in the form of DNS names)
3. An agent password

In production usage, agents will typically authenticate with a single-sign-on, often via their CRM implementation (e.g. Salesforce).

## Integration

## Quick start

Clone this project and replace the GLANCE_GROUP_ID value with your Group ID obtained from Glance.

## Installation

Download the Glance.framework from this project, add it to your Xcode project.
Ensure that Glance.framework is listed within Embedded Binaries and within Linked Libraries and Frameworks.  

### Configuration

Now that you have the Glance framework imported into your Xcode project and have obtained your Glance Group ID you can get start integrating the Visitor SDK.

In your AppDelegate add an import for the Glance Framework headers:

```objc
#import <GlanceFramework/GlanceFramework.h>
```

Initialize the SDK for your group only once, within the AppDelegate applicationDidFinishLaunching method (or on first usage).

Replace GLANCE_GROUP_ID with your own numeric group id.  You should pass an empty string for `token`, this parameter is reserved for future use. Empty strings or any value useful to you can be passed for name, email and phone.  Maximum character lengths are, name: 63, email: 127, phone: 31.

```objc
- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Intitialize Glance Visitor SDK
    [GlanceVisitor init:GLANCE_GROUP_ID token:@"" name:@"" email:@"" phone:@""];
}
```

### Events

Before starting a session you should register a GlanceVisitorDelegate delegate to monitor session start and end events.

The GlanceVisitorDelegate is a protocol with the following definition:

```objc
@protocol GlanceVisitorDelegate
    -(void)glanceVisitorEvent:(GlanceEvent*)event;
@end
```

The glanceVisitorEvent method can receive a variety of GlanceEvent code types.  The important ones for the visitor implementation are: EventConnectedToSession, EventStartSessionFailed and EventSessionEnded.  Here is an example implementation:

```objc
-(void)glanceVisitorEvent:(GlanceEvent*)event{
    switch (event.code)   
    {
        case EventConnectedToSession:
            // Successfully connected to server
            // Get generated session key
            NSString* sessionKey = event.properties[@"sessionkey"];
            break;

        case EventGuestCountChange:
            // agent connected, session now active
            break;

        case EventStartSessionFailed:
            // Couldn't start session
            // Error message can be found in event.message
        break;

        case EventSessionEnded:
            // Session ended
            break;

        default:
            // It is recommended that you at log all other events to help debug during development
            // Best practice is to log all other events of type EventAssertFail, EventError, or EventWarning
            break;
    }
}
```

The delegate can be registered to receive events with the following call:

```objc
[GlanceVisitor addDelegate: self];
```

### Starting a Session

There are two ways to start a session: with and without a session key.  If no session key is provided then one will be generated and returned in the properties of the GlanceEvent with code EventConnectedToSession.

To start a session without a session key:

```objc
[GlanceVisitor startSession];
```

To start a session with a specified session key:

```objc
[GlanceVisitor startSession:@"1234"];
```

### Stopping a Session

To stop a visitor session in progress make the following call:

```objc
[GlanceVisitor endSession];
```

### Masking

Views with personal private information or banking information should be hidden from the agent.  You can mask views like this (masked objects must be of type NSView):

```objc
[GlanceVisitor setMaskedViews: [NSSet setWithObjects: self.creditCardField, self.socialSecurityNumberField, nil]];
```

You can remove masking with another call:

```objc
[GlanceVisitor setMaskedViews: nil];
```

You must remove any masked views before the view itself is deallocated.

#### Changing the Masking Color

Remember, only the agent (screenshare viewer) sees the masking.

The masking color is set in the User Default DMaskingColor in domain com.glancenetworks.Glance.

### Showing a Window or Application

By default `[GlanceVisitor startSession]` will show your application (windows belonging to the running proccess).

The Framework can also be used to show an entire monitor screen, a single window or another application.  You can show windows from other applications, but note that you can only mask NSViews belonging to your application.

The window or process is specified in the` GlanceDisplayParams` object.   You pass these parameters as part of a `StartParams` object to `[GlanceVisitor startSessionWithParams: startParams]` or during a session you can change what is shown with `[GlanceVisitor showDisplay: displayParams]`.

To shown the entire main display screen, use `displayName` "Main":

```objc
GlanceStartParams * startParams = [[GlanceStartParams alloc] init];
startParams->key = @"123456";    // or use @"GLANCE_KEYTYPE_RANDOM" to generate a random key
startParams->displayParams->displayName = @"Main";
[GlanceVisitor startSessionWithParams: startParams];
```

To show a single window pass a [CGWindowID](https://developer.apple.com/documentation/coregraphics/cgwindowid) obtained from `[NSWindow windowNumber]` or functions in [Quartz Window Services](https://developer.apple.com/documentation/coregraphics/quartz_window_services?preferredLanguage=occ).

For example, from an NSWindowController you can call:

```objc
GlanceDisplayParams * dp = [[GlanceDisplayParams alloc] init];

dp.displayName = @"Window";
dp->application.window = self.window.windowNumber;
[GlanceVisitor showDisplay: dp];
```

To show all windows of a process, pass the process id:

```objc
// Show Safari 
dp.displayName = @"Process";
dp->application.process = [NSRunningApplication runningApplicationsWithBundleIdentifier: @"com.apple.Safari"];
```

See the `GlanceFramework` Sample application for examples.


## Testing

### Confirm it's working

To confirm the integration is working, start the session and note the session key.

The agent should go to:
<https://www.glance.net/agentjoin> then login with their Glance username and password.  A form will be shown to enter the session key.

When the key is known a view can be opened directly with
<https://www.glance.net/agentjoin/AgentView.aspx?username={username}&sesionkey={key}&wait=1>

See (other documentation) for details on integrating the agent viewer with other systems and single-sign-on

## Support

Email mobilesupport@glance.net with any questions.
