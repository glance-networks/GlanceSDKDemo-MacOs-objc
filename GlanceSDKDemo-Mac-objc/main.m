//
//  main.m
//  GlanceSDKDemo-Mac-objc
//
//  Created by Ed Hardebeck on 11/2/18.
//  Copyright © 2018 Glance Networks, Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
