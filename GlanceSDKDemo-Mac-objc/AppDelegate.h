//
//  AppDelegate.h
//  GlanceSDKDemo-Mac-objc
//
//  Created by Ed Hardebeck on 11/2/18.
//  Copyright © 2018 Glance Networks, Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <GlanceFramework/GlanceFramework.h>

@interface AppDelegate : NSObject <NSApplicationDelegate, GlanceVisitorDelegate>

- (IBAction)startSession:(id)sender;
- (IBAction)endSession:(id)sender;

@end







